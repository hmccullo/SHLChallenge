"""DWT Processing
"""

import os
import sys
import pywt
import logging
import argparse
from datetime import datetime
from shlChallenge.data import ShlHdf5
from shlChallenge.data import ShlDwt


logger = logging.getLogger(os.path.basename(__file__))


default_data_folder_path = os.path.join(
    os.path.dirname(__file__),
    '../shlData'
)

default_dataset_file_path = os.path.join(
    default_data_folder_path,
    'shl_train.hdf5'
)

default_hdf5_file_path = os.path.join(
    default_data_folder_path,
    'shl_train_dwt.hdf5'
)

default_wavelet = 'db20'


def parse_args():
    parser = argparse.ArgumentParser(
        description="Show animation of SHL dataset."
    )
    parser.add_argument('-o', '--output', type=str,
                        default=default_hdf5_file_path,
                        help="HDF5 filename to store pre-processed data.")
    parser.add_argument('-d', '--hdf5', type=str,
                        default=default_dataset_file_path,
                        help="SHL dataset in HDF5 format.")
    parser.add_argument('-w', '--wavelet', type=str,
                        default=default_wavelet,
                        help="Name of the wavelet to use.")
    parser.add_argument('--log', type=str, default=None,
                        help='Log directory.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Increase output verbosity.')
    return parser.parse_args()


def check_args(args):
    if not os.path.isfile(args.hdf5):
        logger.error('Cannot find hdf5 file at %s.' %
                     args.hdf5)
        sys.exit(1)


def config_logger(log_path=None, verbose=False):
    """Configure logging options.
    """
    if verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    log_handlers = [logging.StreamHandler()]
    if log_path is not None:
        os.makedirs(log_path, exist_ok=True)
        log_filename = '%s_%s.log' % (
            os.path.basename(__file__),
            datetime.now().strftime('%y%m%dT%H%M%S')
        )
        log_handlers.append(
            logging.FileHandler(
                os.path.join(log_path, log_filename)
            )
        )
    logging.basicConfig(
        level=log_level,
        format='[%(asctime)s] %(name)s:%(levelname)s:%(message)s',
        handlers=log_handlers
    )


if __name__ == '__main__':
    args = parse_args()
    check_args(args=args)
    config_logger(log_path=args.log, verbose=args.verbose)
    wavelet = pywt.Wavelet(args.wavelet)
    shlhdf5 = ShlHdf5(args.hdf5)
    shldwt = ShlDwt.from_shlhdf5(args.output, wavelet=wavelet, shlhdf5=shlhdf5)

"""This script downloads SHL Challenge dataset and unzip into data folder.
"""

import os
import wget
import zipfile
import argparse


shlTrainData = [
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/train_acc.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/train_gyr.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/train_mag.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/train_lacc.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/train_gra.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/train_ori.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/train_pressure.zip'
]

shlTrainLabel = [
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/train_label.zip'
]

shlTrainOrder = [
    'http://www.shl-dataset.org/wp-content/uploads/2018/06/train_order.zip'
]

shlTestData = [
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/test_acc.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/test_gyr.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/test_mag.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/test_lacc.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/test_gra.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/test_ori.zip',
    'http://www.shl-dataset.org/wp-content/uploads/SHLChallenge/test_pressure.zip'
]


default_data_path = os.path.join(
    os.path.dirname(__file__),
    '../shlData'
)


def parse_args():
    parser = argparse.ArgumentParser(
        description="Download SHL Challenge datasets."
    )
    parser.add_argument('-o', '--output', type=str, default=default_data_path,
                        help="SHL Dataset Directory")
    return parser.parse_args()


def check_args(args):
    if not os.path.isdir(args.output):
        os.makedirs(args.output, exist_ok=True)


if __name__ == "__main__":
    args = parse_args()
    check_args(args=args)

    archive_folder = os.path.join(args.output, 'archive')
    if not os.path.isdir(archive_folder):
        os.makedirs(archive_folder, exist_ok=True)

    print()
    print('Download and unzip SHL Training Dataset to %s' % args.output)
    for url in shlTrainData:
        print('\t%s' % url)
        filename = wget.download(url=url, out=archive_folder)
        zip_ref = zipfile.ZipFile(file=filename, mode='r')
        zip_ref.extractall(args.output)
        zip_ref.close()
        print('')

    print('Download SHL Training Labels to %s' % args.output)
    for url in shlTrainLabel:
        print('\t%s' % url)
        filename = wget.download(url=url, out=archive_folder)
        zip_ref = zipfile.ZipFile(file=filename, mode='r')
        zip_ref.extractall(args.output)
        zip_ref.close()
        print('')
    for url in shlTrainOrder:
        print('\t%s' % url)
        filename = wget.download(url=url, out=archive_folder)
        zip_ref = zipfile.ZipFile(file=filename, mode='r')
        zip_ref.extractall(args.output)
        zip_ref.close()
        print('')

    print('Download SHL Testing Dataset to %s' % args.output)
    for url in shlTestData:
        print('\t%s' % url)
        filename = wget.download(url=url, out=args.output)
        zip_ref = zipfile.ZipFile(file=filename, mode='r')
        zip_ref.extractall(args.output)
        zip_ref.close()
        print('')

    print('Done!')

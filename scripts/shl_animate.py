import os
import sys
import logging
import argparse
from datetime import datetime
from shlChallenge.data import ShlHdf5
from matplotlib import style as mstyle
mstyle.use('ggplot')


logger = logging.getLogger(os.path.basename(__file__))

default_data_folder_path = os.path.join(
    os.path.dirname(__file__),
    '../shlData'
)

default_hdf5_file_path = os.path.join(
    default_data_folder_path,
    'shl_train.hdf5'
)

default_start = 0
default_length = 12000
default_update = 600


def parse_args():
    parser = argparse.ArgumentParser(
        description="Show animation of SHL dataset."
    )
    parser.add_argument('-d', '--hdf5', type=str,
                        default=default_hdf5_file_path,
                        help="SHL dataset in HDF5 format")
    parser.add_argument('-s', '--start', type=int,
                        default=default_start,
                        help="Index of data sample to start animation.")
    parser.add_argument('-l', '--length', type=int,
                        default=default_length,
                        help="Number of data samples to display in one frame.")
    parser.add_argument('-u', '--update', type=int,
                        default=default_update,
                        help="Number of data samples to load every step in "
                             "animation.")
    parser.add_argument('--log', type=str, default=None,
                        help='Log directory.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Increase output verbosity.')
    return parser.parse_args()


def check_args(args):
    if not os.path.isfile(args.hdf5):
        logger.error('Cannot find hdf5 file at %s.' %
                     args.hdf5)
        sys.exit(1)


def config_logger(log_path=None, verbose=False):
    """Configure logging options.
    """
    if verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    log_handlers = [logging.StreamHandler()]
    if log_path is not None:
        os.makedirs(log_path, exist_ok=True)
        log_filename = '%s_%s.log' % (
            os.path.basename(__file__),
            datetime.now().strftime('%y%m%dT%H%M%S')
        )
        log_handlers.append(
            logging.FileHandler(
                os.path.join(log_path, log_filename)
            )
        )
    logging.basicConfig(
        level=log_level,
        format='[%(asctime)s] %(name)s:%(levelname)s:%(message)s',
        handlers=log_handlers
    )


if __name__ == '__main__':
    args = parse_args()
    check_args(args=args)
    config_logger(log_path=args.log, verbose=args.verbose)
    dataset = ShlHdf5(args.hdf5)
    dataset.animate(length=args.length, update=args.update)

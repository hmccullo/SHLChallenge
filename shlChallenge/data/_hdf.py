"""ShlHdf5 class interperates sensor data into HDF5 format for faster I/O
in training process."""

import os
import logging
import h5py
from collections import OrderedDict
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation


logger = logging.getLogger(__name__)


class ShlHdf5:
    """Translate SHL dataset into hdf5 dataset format.

    Args:
        hdf_file (:obj:`str`): HDF5 filename
        driver (:obj:`str`): Driver to use with h5py File object.
    """
    # Each frame is 1-minute long with data sampled at 100Hz
    FRAME_SIZE = 6000
    # Dictionary for sensor and file names
    SENSOR_INFO = OrderedDict({
        'Accelerometer': {
            'label': 'Acc',
            'axes': ['x', 'y', 'z']
        },
        'Gyroscope': {
            'label': 'Gyr',
            'axes': ['x', 'y', 'z']
        },
        'Magnetometer': {
            'label': 'Mag',
            'axes': ['x', 'y', 'z']
        },
        'Linear accelerometer': {
            'label': 'LAcc',
            'axes': ['x', 'y', 'z']
        },
        'Gravity': {
            'label': 'Gra',
            'axes': ['x', 'y', 'z']
        },
        'Orientation': {
            'label': 'Ori',
            'axes': ['w', 'x', 'y', 'z']
        },
        'Pressure': {
            'label': 'Pressure',
            'axes': None
        }
    })
    # Label List
    ACTIVITIES = [
        'Still', 'Walk', 'Run', 'Bike', 'Car', 'Bus', 'Train', 'Subway'
    ]

    def __init__(self, hdf_file, driver=None):
        self._file = h5py.File(hdf_file, 'r', driver=driver)
        if 'columns' in self._file.attrs:
            self._columns = [
                column_name.decode('utf-8')
                for column_name in self._file.attrs['columns']
            ]
        else:
            self._columns = None
        if 'labels' in self._file.attrs:
            self._labels = [
                label_name.decode('utf-8')
                for label_name in self._file.attrs['labels']
            ]
        else:
            self._labels = None

    def summary(self):
        pass

    @property
    def data(self):
        return self._file['data']

    @property
    def label(self):
        if 'label' in self._file:
            return self._file['label']
        else:
            return None

    @property
    def file(self):
        return self._file

    def column_id(self, column_name):
        """Get the index of the specified column.

        Args:
            column_name (:obj:`str`): Name of the data column.
        """
        if column_name in self._columns:
            return self._columns.index(column_name)
        else:
            return None

    def animate(self, start=0, length=12000, update=100):
        """Animate the data from dataset file

        Args:
            length (:obj:`int`): Window length to plot per timestep
            update (:obj:`int`): Update frequency (in number of data samples)
        """
        label = self.label
        num_subfigs = len(ShlHdf5.SENSOR_INFO)
        subfig_labels = list(ShlHdf5.SENSOR_INFO.keys())
        if label is not None:
            num_subfigs += 1
            subfig_labels.append('label')

        fig, axes = plt.subplots(nrows=num_subfigs, ncols=1,
                                 sharex=True, sharey=False)

        num_samples = self.data.shape[0]
        # Prepare activity colors
        prop_cycle = plt.rcParams['axes.prop_cycle']
        color_reservior = list(prop_cycle.by_key()['color'])

        def animate_init():
            return

        def animate_update(frame):
            for ax in axes:
                ax.cla()
            ani_start = (start + frame * update) % num_samples
            ani_stop = min(ani_start + length, num_samples)

            x_list = list(range(ani_start, ani_stop))

            x_major_ticks = [
                tick for tick in range(ani_start, ani_stop)
                if tick % ShlHdf5.FRAME_SIZE == 0
            ]
            x_minor_ticks = [
                tick for tick in range(ani_start, ani_stop)
                if tick % (ShlHdf5.FRAME_SIZE / 60) == 0
            ]

            plot_data = self.data[ani_start:ani_stop, :]
            plot_list = []

            for i, sensor in enumerate(ShlHdf5.SENSOR_INFO):
                ax = axes[i]
                # If sensor has multiple axes, generate multiple plots
                sensor_acr = ShlHdf5.SENSOR_INFO[sensor]['label']
                sensor_axes = ShlHdf5.SENSOR_INFO[sensor]['axes']
                if sensor_axes is None:
                    col = self.column_id(sensor_acr)
                    plot_list.append(
                        ax.plot(x_list, plot_data[:, col], label=sensor_acr)
                    )
                else:
                    for sensor_ax in sensor_axes:
                        column_name = sensor_acr + '_' + sensor_ax
                        col = self.column_id(column_name)
                        plot_list.append(
                            ax.plot(x_list, plot_data[:, col],
                                    label=column_name)
                        )
                ax.set_ylabel(sensor_acr)
                ax.set_xticks(x_major_ticks, minor=False)
                ax.set_xticks(x_minor_ticks, minor=True)
                ax.set_ylim(auto=True)
                ax.legend(loc="upper left", bbox_to_anchor=(1, 1))

            if 'label' in subfig_labels:
                ax = axes[-1]
                label_data = self.label[ani_start:ani_stop]
                class_list = [label_data[0]]
                class_length_list = [0]
                for label in label_data:
                    if label == class_list[-1]:
                        class_length_list[-1] += 1
                    else:
                        class_list.append(label)
                        class_length_list.append(0)
                left = ani_start
                for label, class_length in zip(class_list, class_length_list):
                    plot_list.append(
                        ax.barh([0], [class_length], 0.75,
                                color=color_reservior[label - 1],
                                left=left)
                    )
                    plot_list.append(
                        ax.text(left + class_length/2, 0,
                                ShlHdf5.ACTIVITIES[label - 1],
                                color='black', fontweight='bold',
                                ha='center', va='center')
                    )
                    left += class_length

            return plot_list

        ani = FuncAnimation(fig, animate_update, init_func=animate_init,
                            frames=range(int(num_samples / update)),
                            interval=10)

        plt.show()


    @staticmethod
    def from_folder(output_file, data_path, label_path=None, order_path=None):
        """Load sensor data from a folder containing training data or testing
        data.

        If the permutation order of the data is provided, reorder the data
        according to the order path.

        Args:
            output_file (:obj:`str`): path to the hdf5 file where the database
                is saved.
            data_path (:obj:`str`): path to sensor data folder.
            label_path (:obj:`str`): path to ground truth labels for sensor
                data.
            order_path (:obj:`str`): path to the order of sensor data.
        """
        # Create HDF5 store and append columns
        logger.debug('Save to .hdf5 file %s' % output_file)
        f = h5py.File(output_file, 'w')
        columns = []
        for sensor in ShlHdf5.SENSOR_INFO:
            sensor_acr = ShlHdf5.SENSOR_INFO[sensor]['label']
            axes = ShlHdf5.SENSOR_INFO[sensor]['axes']
            if axes is None:
                columns.append(sensor_acr)
            else:
                columns += [sensor_acr + '_' + ax for ax in axes]
        ds = f.create_dataset('data', (1, len(columns)),
                              maxshape=(None, len(columns)),
                              chunks=(6000, len(columns)))
        f.attrs['columns'] = [
            column_name.encode('utf-8')
            for column_name in columns
        ]

        args_order = None
        if order_path is not None:
            orders = ShlHdf5._load_order(order_path)
            args_order = np.argsort(orders)
        # Data ordered by sensor info
        col = 0
        for sensor in ShlHdf5.SENSOR_INFO:
            logger.debug('Processing sensor %s' % sensor)
            sensor_acr = ShlHdf5.SENSOR_INFO[sensor]['label']
            axes = ShlHdf5.SENSOR_INFO[sensor]['axes']
            # Load data for each axes
            if axes is None:
                sensor_data = ShlHdf5._load_data_1d(
                    os.path.join(data_path, sensor_acr + '.txt')
                )
                if args_order is not None:
                    sensor_data = sensor_data.reshape(
                        (-1, ShlHdf5.FRAME_SIZE))[args_order, :].reshape(-1)
                ShlHdf5._write_column(ds, col, sensor_data)
                col += 1
            else:
                for ax in axes:
                    column_name = sensor_acr + '_' + ax
                    data_file_path = os.path.join(
                        data_path, column_name + '.txt'
                    )
                    sensor_data = ShlHdf5._load_data_1d(
                        data_file_path
                    )
                    if args_order is not None:
                        sensor_data = sensor_data.reshape(
                            (-1, ShlHdf5.FRAME_SIZE))[args_order, :].reshape(-1)
                    ShlHdf5._write_column(ds, col, sensor_data)
                    col += 1
        f.flush()

        # Load labels
        data_labels = None
        if label_path is not None:
            data_labels = ShlHdf5._load_label(label_path)
            if args_order is not None:
                data_labels = data_labels.reshape(
                    (-1, ShlHdf5.FRAME_SIZE))[args_order, :].reshape(-1)
            label_ds = f.create_dataset('label', data=data_labels,
                                        chunks=(6000, ))
            f.attrs['labels'] = [
                label_name.encode('utf-8')
                for label_name in ShlHdf5.ACTIVITIES
            ]
            f.flush()

        # Create a ShlHdf5 object given the output file
        return ShlHdf5(hdf_file=output_file)


    @staticmethod
    def _write_column(dataset, col, data):
        """Write a column of data to dataset

        Args:
            dataset (:obj:`h5py.dataset`): H5PY dataset object.
            col (:obj:`int`): Column index.
            data (:obj:`numpy.ndarray`): 1-D array to write to the column.
        """
        logger.debug('Write to column %d' % col)
        nrows, ncols = dataset.shape
        data_nrows, = data.shape
        if data_nrows > nrows:
            logger.debug('Reshape dataset to (%d, %d)' % (
                data_nrows, ncols
            ))
            dataset.resize((data_nrows, ncols))
        # Write data to dataset
        dataset[:, col] = data


    @staticmethod
    def _load_label(label_path):
        """ Load ground truth labels into an numpy array.

        Args:
            label_path (:obj:`str`): path to the ground truth labels.

        Returns:
            labels (:obj:`numpy.ndarray`): a 1-D array with class identifiers.
        """
        logger.debug('Loading labels from %s' % label_path)
        label_list = []
        with open(label_path, 'r') as f:
            for line in f:
                tokens = line.strip().split()
                label_list += [int(float(i)) for i in tokens]
        labels = np.array(label_list)
        logger.debug("%d labels loaded." % labels.shape[0])
        return labels

    @staticmethod
    def _load_data_1d(data_file_path):
        """Load 1-D sensor data file.

        Args:
            data_file_path (:obj:`str`): path to the sensor data txt file.

        Returns:
            :obj:`numpy.ndarray`: 1-D float array containing sensor data.
        """
        logger.debug('Loading 1-D data from %s' % data_file_path)
        data_list = []
        with open(data_file_path, 'r') as f:
            for line in f:
                tokens = line.strip().split()
                data_list += [float(i) for i in tokens]
        data = np.array(data_list)
        logger.debug("%d data points loaded from sensors." % data.shape[0])
        return data

    @staticmethod
    def _load_order(order_file_path):
        """Load the order of each frame.

        Args:
            order_file_path (:obj:`str`): Path to the permutation order file.

        Returns:
            :obj:`numpy.ndarray`: 1-D int array containing data permutation
                order.
        """
        logger.debug('Loading frame order from %s' % order_file_path)
        order_list = []
        with open(order_file_path, 'r') as f:
            for line in f:
                token = line.strip()
                order_list.append(int(float(token)))
        order = np.array(order_list)
        logger.debug("Order of %d frames loaded." % order.shape[0])
        return order

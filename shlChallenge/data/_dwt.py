import h5py
import pywt
import logging
import progressbar
import numpy as np
import tensorflow as tf
from ._hdf import ShlHdf5

logger = logging.getLogger(__name__)


class ShlDwt:
    """SHL Dataset Preprocessing - Discrete Wavelet Transformation
    """
    FRAME_SIZE = ShlHdf5.FRAME_SIZE
    ACTIVITIES = ShlHdf5.ACTIVITIES
    SENSOR_INFO = ShlHdf5.SENSOR_INFO

    def __init__(self, hdf_file, driver=None):
        self._file = h5py.File(hdf_file, 'r', driver=driver)
        if 'features' in self._file.attrs:
            self.features = [
                column_name.decode('utf-8')
                for column_name in self._file.attrs['features']
            ]
        else:
            self.features = None
        if 'feature_width' in self._file.attrs:
            self.feature_width = self._file.attrs['feature_width']
        else:
            self.feature_width = None

        if 'labels' in self._file.attrs:
            self._labels = [
                label_name.decode('utf-8')
                for label_name in self._file.attrs['labels']
            ]
        else:
            self._labels = None

    @property
    def data(self):
        return self._file['data']

    @property
    def label(self):
        if 'label' in self._file:
            return self._file['label']
        else:
            return None

    @property
    def frame_label(self):
        if 'frame_label' in self._file:
            return self._file['frame_label']
        else:
            return None

    @property
    def file(self):
        return self._file

    def to_tf_dataset(self, train_indices=None):
        """Return a tensorflow Dataset object for training
        """
        class _Generator:
            def __init__(self, data, label=None, train_indices=None):
                if label is not None:
                    # Make sure that data and label has one-to-one correspondence.
                    assert(data.shape[0] == label.shape[0])
                if train_indices is None:
                    self.train_indices = np.arange(data.shape[0])
                else:
                    self.train_indices = train_indices
                self.data = data
                self.label = label
                self.num_samples = self.train_indices.shape[0]
                self.i = 0

            def __call__(self):
                while True:
                    i = self.i % self.num_samples
                    self.i += 1
                    reshaped_data = self.data[self.train_indices[i], :]
                    if self.label is None:
                        yield reshaped_data
                    else:
                        label = self.label[self.train_indices[i]] - 1
                        yield reshaped_data, label

        generator = _Generator(data=self.data, label=self.frame_label, train_indices=train_indices)
        if self.frame_label is not None:
            return tf.data.Dataset.from_generator(
                generator=generator,
                output_types=(tf.float32, tf.int32),
                output_shapes=(
                    tf.TensorShape(dims=[self.data.shape[1]]),
                    tf.TensorShape(dims=[])
                )
            )
        else:
            return tf.data.Dataset.from_generator(
                generator=generator,
                output_types=tf.float32,
                output_shapes=tf.TensorShape(dims=[self.data.shape[1]])
            )

    @staticmethod
    def from_shlhdf5(output_file, shlhdf5, wavelet=None):
        """Create preprocessed data based on ShlHdf5 class
        """
        assert(isinstance(shlhdf5, ShlHdf5))

        # Create HDF5 store and append columns
        logger.debug('Save to .hdf5 file %s' % output_file)
        f = h5py.File(output_file, 'w')

        if wavelet is None:
            wavelet = pywt.Wavelet('db20')

        # Columns needed for each 1-D axis
        size_array = ShlDwt._multilevel_dwt_coeff_len(
            shlhdf5.FRAME_SIZE, wavelet
        )
        num_columns = np.sum(size_array)

        # Get the number of 1-D axes needed
        sensor_labels = []
        magnitude_columns = {}
        for sensor in shlhdf5.SENSOR_INFO:
            sensor_acr = ShlHdf5.SENSOR_INFO[sensor]['label']
            sensor_axes = shlhdf5.SENSOR_INFO[sensor]['axes']
            if sensor_axes is None:
                sensor_labels.append(sensor_acr)
            else:
                mag_label = sensor_acr + '_mag'
                sensor_labels.append(mag_label)
                magnitude_columns[mag_label] = []
                for sensor_ax in sensor_axes:
                    column_name = sensor_acr + '_' + sensor_ax
                    magnitude_columns[mag_label].append(
                        shlhdf5.column_id(column_name)
                    )
                    sensor_labels.append(column_name)

        logger.debug('Total 1-D columns: %s' % ', '.join(sensor_labels))

        # Calculate dwt for each 1-D array
        num_points = shlhdf5.data.shape[0]
        num_frames = int(num_points / shlhdf5.FRAME_SIZE)

        # Create dataset
        dataset = f.create_dataset(
            'data',
            shape=(num_frames, len(sensor_labels) * num_columns),
            chunks=True,
        )

        # Create Dataset
        with progressbar.ProgressBar(max_value=num_frames) as bar:
            for i in range(num_frames):
                # for each labels, get dwt transformation and addit into dataset
                frame_start = shlhdf5.FRAME_SIZE * i
                frame_end = frame_start + shlhdf5.FRAME_SIZE
                frame_data = shlhdf5.data[frame_start:frame_end, :]
                for j, label in enumerate(sensor_labels):
                    if label in magnitude_columns:
                        selected_data = frame_data[:, magnitude_columns[label]]
                        data_1d_array = np.linalg.norm(selected_data, axis=1)
                    else:
                        column_id = shlhdf5.column_id(label)
                        data_1d_array = frame_data[:, column_id]
                    # Calculate discrete wavelet transformation
                    dwt_result = pywt.wavedec(data_1d_array, wavelet=wavelet,
                                              mode='symmetric')
                    # Concatenate dwt result
                    col_start = j * num_columns
                    col_stop = col_start + num_columns
                    dataset[i, col_start:col_stop] = np.concatenate(
                        dwt_result, axis=0
                    )
                bar.update(i)

        f.attrs['features'] = [
            label_name.encode('utf-8')
            for label_name in sensor_labels
        ]
        f.attrs['feature_width'] = num_columns
        f.attrs['wavelet'] = wavelet.name
        f.flush()

        # Create labels
        if shlhdf5.label is not None:
            label_array = shlhdf5.label.value
            label_array = label_array.reshape((-1, shlhdf5.FRAME_SIZE))
            f.create_dataset('label', data=label_array, chunks=True)
            f.attrs['labels'] = [
                label_name.encode('utf-8')
                for label_name in ShlHdf5.ACTIVITIES
            ]
            majority_label = np.array([
                np.argmax(np.bincount(label_array[i, :]))
                for i in range(label_array.shape[0])
            ])
            f.create_dataset('frame_label', data=majority_label,
                             chunks=True)
            f.flush()

        # Load and return
        return ShlDwt(output_file)

    @staticmethod
    def _multilevel_dwt_coeff_len(data_len, wavelet, mode='symmetric'):
        length = data_len
        size_array = []
        max_layer = pywt.dwt_max_level(data_len=data_len,
                                       filter_len=wavelet.dec_len)
        for i in range(max_layer):
            size_array.append(pywt.dwt_coeff_len(
                data_len=length,
                filter_len=wavelet.dec_len,
                mode=mode
            ))
            length = size_array[-1]

        size_array.append(size_array[-1])
        return size_array

